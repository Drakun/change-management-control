![cmd3.JPG](https://bitbucket.org/repo/Rpn9gx/images/617965645-cmd3.JPG)

# Change Management Control Software #
## Created by: Brian Drake ##

This page is intended to help users install and run the Change Management Control application. This application is used to compare commits between Stash and JIRA to make sure each of those commits is part of an approved change package.

### What you need: ###
* You can download the source code from here, or the compiled .JAR file here.
* You will need JDK 1.7 in order to run this application.
* You will need to know the date format of the desired release you wish to check.
* You will also need to know the project and repository name from Stash that you wish to check against.

### Running with the .JAR file: ###
Open a command prompt and navigate to the directory where you saved the .JAR file. 

![cmd1.JPG](https://bitbucket.org/repo/Rpn9gx/images/2801705514-cmd1.JPG)

Next type in "java -jar CMC.jar" in order to run the program.

![cmd2.JPG](https://bitbucket.org/repo/Rpn9gx/images/3079486304-cmd2.JPG)

You should now see a GUI display box like the one pictured above. Fill out all of the fields and make sure your entries are correct. Once completed, press the "Generate Report" button. After waiting for a short moment you should see the output displayed int the lower text area. If you receive an error double check your entered values and make sure the information is correct.

![cmd3.JPG](https://bitbucket.org/repo/Rpn9gx/images/2515736522-cmd3.JPG)

** The above image is an example of a successful response. ** 

![cmd4.JPG](https://bitbucket.org/repo/Rpn9gx/images/2631746077-cmd4.JPG)

** The above image is an example of a unsuccessful response. **

This information can be copied and pasted directly into an excel spread sheet. There might be some formatting issues with some of the entries, as you can see in the above example there is an additional space character. However, this should not occur very often.
Running from the source code:
Copy the source code from the GIT repo and open your IDE.
Import the project and check for any compilation errors. 
The main method is part of the GUI.java class.
Run the program using the same steps as above.