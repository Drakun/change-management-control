/*Written by: Brian Drake
 *The supreme overlord of interning
 *Availity 2015  
 */

package com.availity.CMC;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.security.GeneralSecurityException;
import java.security.cert.X509Certificate;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.commons.codec.binary.Base64;

public class Connection 
{
	private static String userName = null;
	private static String password = null;
	
	private static String result = null;
	private static String authString = null;
	
	public Connection()
	{
		System.out.println("Starting Connection Object");
		final TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
	        @Override
	        public void checkClientTrusted( final X509Certificate[] chain, final String authType ) {
	        }
	        @Override
	        public void checkServerTrusted( final X509Certificate[] chain, final String authType ) {
	        }
	        @Override
	        public X509Certificate[] getAcceptedIssuers() {
	            return null;
	        }
	    } };
		// Install the all-trusting trust manager
		try {
		    SSLContext sc = SSLContext.getInstance("SSL"); 
		    sc.init(null, trustAllCerts, new java.security.SecureRandom()); 
		    HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
		} 
	}
	public void connect(String connection, int start) throws IOException
	{
		System.out.println("Make Connection");
		// Now you can access an https URL without having the certificate in the truststore
		try {
			authString = userName + ":" + password;
			String connect = connection + start;
			//String jiraAPI = "https://jira.availity.com/rest/api/latest/search?jql=fixversion%20%3D%20'2015.0819%20Gateway'&maxResults=1000&startAt=" + start;
			String authStringConnect = authString;

			byte[] authEncBytesCon = Base64.encodeBase64(authStringConnect.getBytes());
			String authStringEnc = new String(authEncBytesCon);

			URL url = new URL(connect);
			URLConnection urlConnection = url.openConnection();
			urlConnection.setRequestProperty("Authorization", "Basic " + authStringEnc);
			InputStream is = urlConnection.getInputStream();
			InputStreamReader isr = new InputStreamReader(is);
			
			int numCharsRead;
			char[] charArray = new char[1024];
			StringBuffer sb = new StringBuffer();

			while ((numCharsRead = isr.read(charArray)) > 0) {
				sb.append(charArray, 0, numCharsRead);
			}
			result = sb.toString();
			
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
	}
	
	public String getResult()
	{
		return result;
	}
	
	public void setName(String userName)
	{
		this.userName = userName;
	}
	
	public void setPassword(String password)
	{
		this.password = password.toString();
	}
}
