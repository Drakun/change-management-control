package com.availity.CMC;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class GetData 
{
	private static Integer nextPage = 0;
	
	private String jiraDate = null;
	private String stashOffcycleDate = null;
	private String stashReleaseDate = null;
	
	private ArrayList stories = new ArrayList<String>();
	private ArrayList tags = new ArrayList<String>();
	private ArrayList jiraStories = new ArrayList<String>();
	
//	private String axiURL = "https://git.availity.com/rest/api/1.0/projects/AXI/repos/axi/compare/commits?from=Offcycle_20150819&to=Release_20150815&limit=1000&start=";
//	private String ariesURL = "https://git.availity.com/rest/api/1.0/projects/ARIES/repos/aries/compare/commits?from=Offcycle_20150819&to=Release_20150815&limit=1000&start=";
//	private String availity_webURL = "https://git.availity.com/rest/api/1.0/projects/SS/repos/availity-web/compare/commits?from=Offcycle_20150819&to=Release_20150815&limit=1000&start=";
	
	//Connection start = new Connection();
	
	public void getData(String project, String repo, String from, String to, Connection start) throws JSONException, IOException
	{ 
		System.out.println("Get Stash Data");
		String url = "https://git.availity.com/rest/api/1.0/projects/" + project + "/repos/" + repo + "/compare/commits?from=" + from + "_" + stashOffcycleDate + "&to=" + to + "_" + stashReleaseDate 
				+ "&limit=1000&start=";
		start.connect(url,nextPage);
		String resultStash = start.getResult();
		String merge = "";
		
		JSONObject json = new JSONObject(resultStash);
		JSONArray values = json.getJSONArray("values");
		
		if(!json.getBoolean("isLastPage"))
		{
			nextPage = json.getInt("nextPageStart");
		}
		int count = 0;
		
		json = new JSONObject(resultStash);
		values = json.getJSONArray("values");
			
		for (int i = 0; i < values.length(); i++)
		{
			merge = "";
			try{
				merge = values.getJSONObject(i).getString("message");
				if(!merge.subSequence(0, 5).equals("Merge"))
				{
					if(values.getJSONObject(i).getJSONObject("attributes").getJSONArray("jira-key") != null)
					{
						JSONArray jiraKey = values.getJSONObject(i).getJSONObject("attributes").getJSONArray("jira-key");
						JSONObject jsonID = values.getJSONObject(i);
						JSONObject jsonName = values.getJSONObject(i);
						JSONObject jsonDate = values.getJSONObject(i);
						Date date = new Date(jsonDate.getLong("authorTimestamp"));
						
						for (int j = 0; j < jiraKey.length(); j++)
						{
							stories.add(jiraKey.getString(j));
							tags.add(values.getJSONObject(i).getString("message") + " \t " + jsonName.getJSONObject("author").getString("displayName") + " \t " + jsonID.getString("id"));
							count++;
						}
					}
				}
				else
				{
					continue;
				}
			}
			catch(Exception e){
			}
		}
		if(!json.getBoolean("isLastPage"))
		{
			getData(project, repo, from, to, start);
		}
		//System.out.println(count + " " + axiStories.size());
	}
	
	public void getJiraData(Connection start) throws JSONException, IOException
	{ 
		System.out.println("Get Jira Data");
		String jiraURL = "https://jira.availity.com/rest/api/latest/search?jql=fixversion%20%3D%20'" + jiraDate + "%20Gateway'&maxResults=1000&startAt=";
		start.connect(jiraURL,nextPage);
		String resultJira = start.getResult();
		
		JSONObject json = new JSONObject(resultJira);
		JSONArray issues = json.getJSONArray("issues");
		
		if(json.getInt("total") > json.getInt("maxResults"))
		{
			nextPage += 1000;
		}
		int count = 0;
			
		for (int i = 0; i < issues.length(); i++)
		{
			try{
				if(issues.getJSONObject(i).getJSONObject("fields").getJSONArray("issuelinks") != null)
				{
					JSONArray outwardIssues = issues.getJSONObject(i).getJSONObject("fields").getJSONArray("issuelinks");
					
					for (int j = 0; j < outwardIssues.length(); j++)
					{
						jiraStories.add(outwardIssues.getJSONObject(j).getJSONObject("outwardIssue").getString("key"));
						//System.out.println(outwardIssues.getJSONObject(j).getJSONObject("outwardIssue").getString("key"));
						count++;
					}
				}
			}
			catch(Exception e){
			}
		}
		if(json.getInt("total") > json.getInt("maxResults"))
		{
			getJiraData(start);
		}
	}
	
	public ArrayList<String> getStories()
	{
		return stories;
	}
	
	public ArrayList<String> getTags()
	{
		return tags;
	}
	
	public ArrayList<String> getJiraStories()
	{
		return jiraStories;
	}
	
	public void setJiraDate(String date)
	{
		jiraDate = date.substring(0, 4) + "." + date.substring(4, date.length()); 
	}
	
	public void setStashOffcycleDate(String Offcycledate)
	{
		stashOffcycleDate = Offcycledate; 
	}
	
	public void setStashReleaseDate(String ReleaseDate)
	{
		stashReleaseDate = ReleaseDate; 
	}
}
