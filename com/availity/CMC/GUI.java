package com.availity.CMC;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTextField;

import java.awt.Cursor;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;

import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.BoxLayout;
import javax.swing.JComboBox;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.awt.Font;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.JPasswordField;
import javax.swing.JTextPane;
import javax.swing.JTextArea;

import org.json.JSONException;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JProgressBar;
import javax.swing.JToggleButton;
import javax.swing.JRadioButton;

public class GUI extends JFrame {

	private JFrame frmChangeManagementControl;
	private JTextField userNameTextField;
	private JTextField offcycleTextField;
	private JTextField releaseTextField;
	private JPasswordField passwordField;
	private Cursor waitCursor = new Cursor(Cursor.WAIT_CURSOR);
	private Cursor defaultCursor = new Cursor(Cursor.DEFAULT_CURSOR);

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUI window = new GUI();
					window.frmChangeManagementControl.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public GUI() {
		System.out.println("Start GUI");
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		System.out.println("Create Frame");
		frmChangeManagementControl = new JFrame();
		frmChangeManagementControl.setTitle("Change Management Control");
		frmChangeManagementControl.setBounds(100, 100, 796, 525);
		frmChangeManagementControl.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmChangeManagementControl.getContentPane().setLayout(null);
		
		System.out.println("Create Combo Boxs");
		final JComboBox projectNameComboBox = new JComboBox();
		projectNameComboBox.setToolTipText("ex: AXI");
		//projectNameComboBox.setModel(new DefaultComboBoxModel(new String[] {"ACX", "API", "ARIES", "AVA", "ACC", "LDS", "AXI", "CM", "CS", "DBM", "ECTPL", "FLIPPER", "MT", "OCS", "PA", "QWKDEM", "RCM", "SXP", "SS", "TS", "UNX", "WA", "XBD"}));
		projectNameComboBox.setBounds(301, 79, 131, 20);
		frmChangeManagementControl.getContentPane().add(projectNameComboBox);
		
		final JComboBox repoComboBox = new JComboBox();
		repoComboBox.setToolTipText("ex: axi");
		//repoComboBox.setModel(new DefaultComboBoxModel(new String[] {"availity", "availity-static-content", "availity-marketing", "claims-replication", "flipper", "anthem-fee-scheduling", "availity-angular-sdk", "availity-cli", "availity-docs", "availity-dotfiles", "availity-ekko", "availity-generator", "availity-gulp", "availity-gulp-starter", "availity-kamino", "availity-react", "availity-search", "availity-toolkit", "availity-uikit", "availity-workflow", "availity-yoda", "linkmerge", "aries", "aries-cleanup", "aries-messaging-bridge", "dlqreport", "mirth-channels", "vitria-legacy", "ava", "availity.com", "availity-marketing", "availity-static-content", "availity-static-content-lfs", "walkme", "axi", "admin", "cmd", "developer_config", "glu-scripts", "gradle-plugins", "security", "ImplementationTools", "website-automation", "sql", "angular-directives-demo", "duplicate-util", "edm", "payer-list-batch-process", "api-inbound-services", "api-outbound-services", "aries-datapower-proxies", "codeset_update", "common-services-inbound", "common-services-internal-outbound", "common-services-outbound", "core2-services", "crservice", "datapower", "datapower_tools", "hl7-mllp-proxy", "iti-acp-inbound", "iti-acp-outbound", "mirth-utilities", "mqconfigs", "mqtools", "securetransport", "utility-services", "availity-toolkit-ts", "billing-system", "dev-environment", "ocs", "ocs-lib", "ptdptool", "stash-crucible-demo", "availity-patientaccess-core", "availity-patientaccess-dataadmin-ssis", "availity-patientaccess-ssrs", "availity-patientaccess-winservices", "availity-patientaccess-x12", "capacitytopayapi", "pa-dummy-repo", "projectmirth", "smartcycle", "smartcycledataadmintool", "winservices", "x12parser", "axi-test-handlers", "automerge-test", "jenkins-testing", "rcm-dummy-sandbox", "test2", "testrepo", "training", "availity-web", "axi-navbar", "navigation", "pinta-config", "provider-data-management", "prt-server-config", "uft-automation", "automated-tests", "functional-test", "testservices_sandbox", "udemy", "cfengine", "action-center", "availity-assets", "clinical-administration", "clinical-authorizations", "eligibility", "enterprise-configuration-tool", "insights-dashboard", "manage-claims", "medical-attachments", "patient-assessment-forms", "payer-resources", "payer-spaces-anthem", "spaces", "unmatched-claim-responses", "xbuddy", "xbuddy-gui", "xbuddy-web"}));
		repoComboBox.setBounds(609, 79, 131, 20);
		frmChangeManagementControl.getContentPane().add(repoComboBox);
		
		JLabel lblFrom = new JLabel("From:");
		lblFrom.setBounds(149, 53, 46, 14);
		frmChangeManagementControl.getContentPane().add(lblFrom);
		
		JLabel lblTo = new JLabel("To:");
		lblTo.setBounds(457, 53, 46, 14);
		frmChangeManagementControl.getContentPane().add(lblTo);
		
		System.out.println("Create Radio Buttons");
		final JRadioButton rdbtnAvaility = new JRadioButton("Availity");
		rdbtnAvaility.setBounds(34, 46, 109, 23);
		frmChangeManagementControl.getContentPane().add(rdbtnAvaility);
		
		final JRadioButton rdbtnRcm = new JRadioButton("RCM");
		rdbtnRcm.setBounds(34, 78, 109, 23);
		frmChangeManagementControl.getContentPane().add(rdbtnRcm);
		
		ButtonGroup group = new ButtonGroup();
		group.add(rdbtnAvaility);
		group.add(rdbtnRcm);
		
		rdbtnAvaility.addActionListener(new ActionListener() 
		{
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				JRadioButton button = (JRadioButton) e.getSource();
				 
		        if (button == rdbtnAvaility) 
		        {
		        	repoComboBox.setModel(new DefaultComboBoxModel(new String[] {"availity", "availity-static-content", "availity-marketing", "claims-replication", "flipper", "anthem-fee-scheduling", "availity-angular-sdk", "availity-cli", "availity-docs", "availity-dotfiles", "availity-ekko", "availity-generator", "availity-gulp", "availity-gulp-starter", "availity-kamino", "availity-react", "availity-search", "availity-toolkit", "availity-uikit", "availity-workflow", "availity-yoda", "linkmerge", "aries", "aries-cleanup", "aries-messaging-bridge", "dlqreport", "mirth-channels", "vitria-legacy", "ava", "availity.com", "availity-marketing", "availity-static-content", "availity-static-content-lfs", "walkme", "axi", "admin", "cmd", "developer_config", "glu-scripts", "gradle-plugins", "security", "ImplementationTools", "website-automation", "sql", "angular-directives-demo", "duplicate-util", "edm", "payer-list-batch-process", "api-inbound-services", "api-outbound-services", "aries-datapower-proxies", "codeset_update", "common-services-inbound", "common-services-internal-outbound", "common-services-outbound", "core2-services", "crservice", "datapower", "datapower_tools", "hl7-mllp-proxy", "iti-acp-inbound", "iti-acp-outbound", "mirth-utilities", "mqconfigs", "mqtools", "securetransport", "utility-services", "availity-toolkit-ts", "billing-system", "dev-environment", "ocs", "ocs-lib", "ptdptool", "stash-crucible-demo", "availity-patientaccess-core", "availity-patientaccess-dataadmin-ssis", "availity-patientaccess-ssrs", "availity-patientaccess-winservices", "availity-patientaccess-x12", "capacitytopayapi", "pa-dummy-repo", "projectmirth", "smartcycle", "smartcycledataadmintool", "winservices", "x12parser", "axi-test-handlers", "automerge-test", "jenkins-testing", "rcm-dummy-sandbox", "test2", "testrepo", "training", "availity-web", "axi-navbar", "navigation", "pinta-config", "provider-data-management", "prt-server-config", "uft-automation", "automated-tests", "functional-test", "testservices_sandbox", "udemy", "cfengine", "action-center", "availity-assets", "clinical-administration", "clinical-authorizations", "eligibility", "enterprise-configuration-tool", "insights-dashboard", "manage-claims", "medical-attachments", "patient-assessment-forms", "payer-resources", "payer-spaces-anthem", "spaces", "unmatched-claim-responses", "xbuddy", "xbuddy-gui", "xbuddy-web"}));
		        	projectNameComboBox.setModel(new DefaultComboBoxModel(new String[] {"ACX", "API", "ARIES", "AVA", "ACC", "LDS", "AXI", "CM", "CS", "DBM", "ECTPL", "FLIPPER", "MT", "OCS", "PA", "QWKDEM", "RCM", "SXP", "SS", "TS", "UNX", "WA", "XBD"}));
		        	frmChangeManagementControl.repaint();
		        } 
			}
		});
		rdbtnRcm.addActionListener(new ActionListener() 
		{
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				JRadioButton button = (JRadioButton) e.getSource();
				 
		        if (button == rdbtnRcm) 
		        {
		        	repoComboBox.setModel(new DefaultComboBoxModel(new String[] {"alvin","bigbin","bootstrap-rmdev","dummy-repo","icd10","packages-choco","rcm-analytics","rcm-angular","rcm-api","rcm-base-libraries","rcm-batch-queues","rcm-businesstools","rcm-claim-status","rcm-claim-translation","rcm-client-rules","rcm-core","rcm-documentwriting","rcm-dotfiles","rcm-dummy-test","rcm-edi","rcm-elig","rcm-framework","rcm-patientportal","rcm-realclean","rcm-remits","rcm-reports","rcm-rerere","rcm-screen-surfer","rcm-scripts","rcm-sfe","rcm-stmts","rcm-utilities","rcm-web","rcm-web-admin","rcm-web-claims","rcm-web-eligibility","rcm-web-ng","rcm-web-payments","rcm-web-realclean","rcm-web-remits","rcm-web-reporting","rcm-web-statements","release-mgmt","rmcorpapp02","tfs2git","tibco","tibco-builds","vss2git"}));
		        	projectNameComboBox.setModel(new DefaultComboBoxModel(new String[] {"RCM"}));
		        	frmChangeManagementControl.repaint();
		        }
			}
		});
		
		JLabel lblPlatform = new JLabel("Platform");
		lblPlatform.setBounds(34, 25, 85, 14);
		frmChangeManagementControl.getContentPane().add(lblPlatform);
		
		JLabel lblUserName = new JLabel("User Name");
		lblUserName.setBounds(205, 25, 86, 14);
		frmChangeManagementControl.getContentPane().add(lblUserName);
		
		System.out.println("Create Textfields");
		userNameTextField = new JTextField();
		userNameTextField.setBounds(301, 22, 131, 20);
		frmChangeManagementControl.getContentPane().add(userNameTextField);
		userNameTextField.setColumns(10);
		
		JLabel lblPassword = new JLabel("Password");
		lblPassword.setBounds(514, 25, 85, 14);
		frmChangeManagementControl.getContentPane().add(lblPassword);
		
		offcycleTextField = new JTextField();
		offcycleTextField.setToolTipText("Format: YearMonthDay ex: 20150819");
		offcycleTextField.setBounds(301, 50, 131, 20);
		frmChangeManagementControl.getContentPane().add(offcycleTextField);
		offcycleTextField.setColumns(10);
		
		releaseTextField = new JTextField();
		releaseTextField.setToolTipText("Format: YearMonthDay ex: 20150819");
		releaseTextField.setBounds(609, 50, 131, 20);
		frmChangeManagementControl.getContentPane().add(releaseTextField);
		releaseTextField.setColumns(10);
		
		JLabel lblProjectName = new JLabel("Project Name");
		lblProjectName.setBounds(205, 83, 86, 14);
		frmChangeManagementControl.getContentPane().add(lblProjectName);
		
		JLabel lblRepo = new JLabel("Repo");
		lblRepo.setBounds(513, 83, 46, 14);
		frmChangeManagementControl.getContentPane().add(lblRepo);
		
		final JComboBox offcycleComboBox = new JComboBox();
		offcycleComboBox.setToolTipText("ex: AXI");
		offcycleComboBox.setModel(new DefaultComboBoxModel(new String[] {"Offcycle","Release"}));
		offcycleComboBox.setBounds(205, 50, 86, 20);
		frmChangeManagementControl.getContentPane().add(offcycleComboBox);
		
		final JComboBox releaseComboBox = new JComboBox();
		releaseComboBox.setToolTipText("ex: AXI");
		releaseComboBox.setModel(new DefaultComboBoxModel(new String[] {"Offcycle","Release"}));
		releaseComboBox.setBounds(513, 50, 86, 20);
		frmChangeManagementControl.getContentPane().add(releaseComboBox);
		
		/*JLabel lblOffcycle = new JLabel("Offcycle :");
		lblOffcycle.setBounds(22, 129, 86, 14);
		frmChangeManagementControl.getContentPane().add(lblOffcycle);
		
		JLabel lblRelease = new JLabel("Release :");
		lblRelease.setBounds(272, 129, 86, 14);
		frmChangeManagementControl.getContentPane().add(lblRelease);*/
		
		passwordField = new JPasswordField();
		passwordField.setBounds(609, 22, 131, 20);
		frmChangeManagementControl.getContentPane().add(passwordField);
		
		System.out.println("Create Text Area");
		final JTextArea output = new JTextArea();
		JScrollPane scroll = new JScrollPane(output);
		output.setEditable(false);
		scroll.setBounds(10, 169, 760, 307);
		
		scroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		scroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);;
		frmChangeManagementControl.getContentPane().add(scroll);
		
		System.out.println("Create Submit Button");
		JButton btnGenerateReport = new JButton("Generate Report");
		btnGenerateReport.setForeground(Color.WHITE);
		btnGenerateReport.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				frmChangeManagementControl.setCursor(waitCursor);
				Connection start = new Connection();
				start.setName(userNameTextField.getText());
				start.setPassword(passwordField.getText());
				
				GetData data = new GetData();
				data.setJiraDate(offcycleTextField.getText());
				data.setStashOffcycleDate(offcycleTextField.getText());
				data.setStashReleaseDate(releaseTextField.getText());
				
				try 
				{
					data.getJiraData(start);
					data.getData(projectNameComboBox.getSelectedItem().toString(), repoComboBox.getSelectedItem().toString(), offcycleComboBox.getSelectedItem().toString(), releaseComboBox.getSelectedItem().toString(), start);
					
					ArrayList<String> notFound = new ArrayList<>();
					ArrayList<String> stories = data.getStories();
					ArrayList<String> tags = data.getTags();
					ArrayList<String> jiraStories = data.getJiraStories();
					
					for(int i = 0; i < stories.size(); i++)
					{
						if(!jiraStories.contains(stories.get(i)))
						{
							notFound.add(stories.get(i) + " \t " + tags.get(i));
						}
					}
					
					frmChangeManagementControl.setCursor(defaultCursor);
					StringBuffer sb = new StringBuffer();
					String dataOutput = null;
					for(int i = 0; i < notFound.size(); i++)
					{
						sb.append((String) notFound.get(i));
						sb.append("\n");
						//output.setText((String) notFound.get(i));
					}
					dataOutput = sb.toString();
					output.setText(dataOutput);
				} 
				catch (JSONException e) 
				{
					e.printStackTrace();
				} 
				catch (IOException e) 
				{
					output.setText("File not found. \nCheck and make sure the information added is correct. \nAlso make sure your username and password is entered correctly.");
					e.printStackTrace();
				}
			}
		});
		btnGenerateReport.setBounds(311, 122, 165, 35);
		Color myColor = Color.decode("#2261b5");
		btnGenerateReport.setBackground(myColor);
		frmChangeManagementControl.getContentPane().add(btnGenerateReport);
	}
}
